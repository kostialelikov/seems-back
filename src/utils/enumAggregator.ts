export default (obj: any) => {
  if (!obj){
    return false
  }
  const arr = Object.keys(obj)
  const values = arr.map((v: any) => obj[v])
  return values.join()
}