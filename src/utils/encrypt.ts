const bcrypt = require('bcryptjs');


const SALT_WORK_FACTOR = 11;

export const compareEncryptedPassword = (password: string, encryptedPassword: string) => {
  return bcrypt.compareSync(password, encryptedPassword);
};

export const encryptPassword = (password: string) => new Promise<string>((resolve, reject) => {
  bcrypt.genSalt(SALT_WORK_FACTOR, (err: Error, salt: string) => {
    if (err) {
      return reject(err);
    }

    bcrypt.hash(password, salt, (errHash: Error, hash: string) => {
      if (errHash) {
        return reject(errHash);
      }

      resolve(hash);
    });
  });
});
