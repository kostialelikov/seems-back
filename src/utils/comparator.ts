type Difference = {
  [key: string]: {
    firstValue: any,
    secondValue: any,
  }
}

export function getDifferences(obj1: any, obj2: any) {
  if (typeof obj1 !== typeof obj2) {
    throw new Error('Objects incomparable');
  }
  const keys = Object.keys(obj1);
  const difference: Difference = {};
  keys.forEach((key) => {
    if (obj1[key] as string != obj2[key] as string) {
      difference[key] = {
        firstValue: obj1[key],
        secondValue: obj2[key]
      }
    }
  });
  return difference;
}