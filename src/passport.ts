import {Strategy as LocalStrategy} from 'passport-local'
import {ExtractJwt, Strategy as JWTStrategy} from 'passport-jwt'
import {User} from './db'
import {compareEncryptedPassword} from './utils/encrypt'

export const jwtSecret: string = String(process.env.JWT_KEY)
    || ([...Array(32)].map(i => (~~(Math.random() * 36)).toString(36)).join(""));

export const localStrategy = new LocalStrategy({
    session: false,
    usernameField: 'email'
}, async (email, password, done) => {
    const user = await User.findOne({
        email
    });
    if (!user) {
        return done('error.email.password.incorrect', undefined)
    }
    if (!compareEncryptedPassword(password, user.password)) {
        return done('error.email.password.incorrect', false)
    }
    return done(null, {...user.toJSON()});
})

export const jwtStrategy = new JWTStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret,
}, async (payload, done) => {
    return done(null, payload)
})
