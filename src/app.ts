import express from 'express'
import {connectMongoDB} from './db'
import passport from 'passport'
import bodyParser from 'body-parser'
import {jwtStrategy, localStrategy} from './passport'
import cors from 'cors'
import router from "./routes";

connectMongoDB().catch(console.error);

passport.use(localStrategy);
passport.use(jwtStrategy);

const app = express()
app.use(passport.initialize())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(cors())
app.use('/api', router);

app.set("port", process.env.PORT || 3000);

export default app;