import {RequestHandler, Router} from "express";
import passport from "passport";
import jwt from "jsonwebtoken";
import httpStatus from "http-status";
import {jwtSecret} from "../../passport";
import {User} from "../../db";
import {IUser, UserRole} from "../../db/interfaces";
import Joi from "joi";
import {encryptPassword} from "../../utils/encrypt";
import {validate} from "express-validation";

const router = Router();

const responseLoginToken: RequestHandler = (req, res) => {
    const {user} = req;
    const token = jwt.sign(user as object, jwtSecret);

    return res.json({user, token});
};

const responseRegistrationTokenValidationSchema = {
    body: {
        email: Joi.string().email().required(),
        password: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required()
    }
}

const responseRegistrationToken: RequestHandler = async (req, res) => {
    try {
        const password = await encryptPassword(req.body.password);
        const user = await User.create({
            ...req.body,
            role: UserRole.REGISTERER,
            password
        });
        if (!user) {
            res.status(httpStatus.INTERNAL_SERVER_ERROR).send('User creation failed');
        }
        const token = jwt.sign(user as object, jwtSecret);
        return res.json({user, token});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
};

router.post('/sign-up', validate(responseRegistrationTokenValidationSchema), responseRegistrationToken);

router.post('/sign-in',
    passport.authenticate('local', {session: false}),
    responseLoginToken)

router.post('/sign-out', passport.authenticate('jwt', {session: false}), (req, res) => {
    return res.json()
})

const getUser: RequestHandler = async (req, res) => {
    try {
        const user = req.user as IUser;
        const u = await User.findOne({
            _id: user._id
        });
        if (!u) {
            return res.status(400).send('An error occupied')
        }
        res.status(httpStatus.OK).send({
            user: {
                ...u.toJSON(),
            }
        })
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
};

router.get('/', passport.authenticate('jwt', {session: false}), getUser);

export default router;