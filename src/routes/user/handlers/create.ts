import {RequestHandler, Router} from "express";
import httpStatus from "http-status";
import Joi from '@hapi/joi';
import enumAggregator from "../../../utils/enumAggregator";
import {User} from "../../../db";
import {encryptPassword} from "../../../utils/encrypt";
import {UserRole} from "../../../db/interfaces";

export const validationSchema = {
    body: {
        email: Joi.string().email().required(),
        password: Joi.string().required(),
        role: Joi.string().allow(enumAggregator(UserRole)).default('user')
    }
}

export const handler: RequestHandler = async (req, res) => {
    try {
        const body = req.body;
        const password = await encryptPassword(body.password);
        const created = await User.create({...body, password});
        res.status(httpStatus.CREATED).send({
            user: created._id
        });
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
};