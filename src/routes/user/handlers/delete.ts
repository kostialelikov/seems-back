import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {User} from "../../../db";
import httpStatus from "http-status";

export const validationSchema = {
    params: {
        id: Joi.string().required()
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const deleted = await User.findByIdAndRemove({
            _id: req.params.id
        });
        res.status(httpStatus.CREATED).send({user: deleted ? deleted._id : null});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
