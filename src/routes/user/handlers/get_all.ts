import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {User} from "../../../db";
import httpStatus from "http-status";
import {UserRole} from "../../../db/interfaces";

export const validationSchema = {
    query: {
        perPage: Joi.number().min(1).default(7),
        page: Joi.number().min(1).default(1),
        search: Joi.string().optional().allow('').default('')
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const procedures = [];
        if (req.query.search) {
            procedures.push({
                $match: {
                    $or: [
                        {firstName: {$regex: req.query.search}},
                        {lastName: {$regex: req.query.search}},
                    ]
                }
            })
        }
        procedures.push({
            $match: {
                role: UserRole.REGISTERER
            }
        })
        const users = await User.aggregate([
            ...procedures,
            {$skip: (Number(req.query.page) - 1) * Number(req.query.perPage)},
            {$limit: Number(req.query.perPage)}
        ])
        const total = await User.aggregate([
            ...procedures,
            {
                $count: 'count'
            }
        ])

        res.status(httpStatus.CREATED).send({users, ...req.query, total: (total && total[0]) ? total[0].count : 0});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
