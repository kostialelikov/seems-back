import {Router} from "express";
import passport from "passport";
import {validate} from 'express-validation';
import validateRole from "../../middlewares/validateRole";
import {UserRole} from "../../db/interfaces";
import * as create from './handlers/create';
import * as deleted from './handlers/delete';
import * as getAll from './handlers/get_all';

const router = Router();

router.post('/',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.ADMIN]),
    // validate(create.validationSchema),
    create.handler
);

router.delete('/:id',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.ADMIN]),
    // validate(deleted.validationSchema),
    deleted.handler
);

router.get('/',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.ADMIN]),
    // validate(getAll.validationSchema),
    getAll.handler
);

export default router;