import {Router} from "express";
import passport from "passport";
import validateRole from "../../middlewares/validateRole";
import {UserRole} from "../../db/interfaces";
import {validate} from "express-validation";
import * as create from './handlers/create'
import * as edit from './handlers/edit';
import * as deleted from './handlers/delete';
import * as getAll from './handlers/get_all';
import * as get from './handlers/get';

const router = Router();

router.post('/',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.ADMIN, UserRole.REGISTERER]),
    // validate(create.validationSchema),
    create.handler
);

router.put('/:id',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.ADMIN, UserRole.REGISTERER]),
    // validate(edit.validationSchema),
    edit.handler
);

router.delete('/:id',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.ADMIN, UserRole.REGISTERER]),
    // validate(deleted.validationSchema),
    deleted.handler
);

router.get('/',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.ADMIN, UserRole.REGISTERER]),
    // validate(getAll.validationSchema),
    getAll.handler
);

router.get('/:id',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.ADMIN, UserRole.REGISTERER]),
    // validate(get.validationSchema),
    get.handler
);

export default router;

