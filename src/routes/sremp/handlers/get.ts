import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {SREMP} from "../../../db";
import httpStatus from "http-status";

export const validationSchema = {
    params: {
        id: Joi.string().required()
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const sremp = await SREMP.findOne({
            _id: req.params.id
        });
        res.status(httpStatus.CREATED).send({sremp: sremp ? sremp.toJSON() : null});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
