import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {ActionLog, SREMP} from "../../../db";
import httpStatus from "http-status";
import {ActionLogType, IUser} from "../../../db/interfaces";
import {getDifferences} from '../../../utils/comparator'

export const validationSchema = {
    params: {
        id: Joi.string().required()
    },
    body: {
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        middleName: Joi.string().required(),
        encumbranceType: Joi.string().required(),
        encumbranceModel: Joi.string().required(),
        encumbranceReason: Joi.string().required(),
        encumbranceObject: Joi.string().required(),
        registrationType: Joi.string().required(),
        registrationDate: Joi.date().required(),
        registrationAgency: Joi.string().required(),
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const user = req.user as IUser;
        const body = req.body;
        const updated = await SREMP.findOneAndUpdate({
            _id: req.params.id
        }, {
            ...body
        });
        if (updated) {
            const difference = getDifferences({
                ...body,
                createdAt: undefined,
                updatedAt: undefined,
                registerer: undefined,
            }, {
                ...updated.toJSON(),
                createdAt: undefined,
                updatedAt: undefined,
                registerer: undefined,
            });
            const actionLogs = Object.keys(difference).map((diff) => {
                return {
                    performer: user._id,
                    target: updated._id,
                    type: ActionLogType.UPDATE,
                    key: diff,
                    oldValue: difference[diff].secondValue,
                    newValue: difference[diff].firstValue,
                }
            });
            await ActionLog.bulkWrite(actionLogs);
        }
        res.status(httpStatus.CREATED).send({sremp: updated ? updated._id : null});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
