import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {ActionLog, SREMP} from "../../../db";
import httpStatus from "http-status";
import {ActionLogType, IUser} from "../../../db/interfaces";

export const validationSchema = {
    params: {
        id: Joi.string().required()
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const user = req.user as IUser;
        const deleted = await SREMP.findByIdAndRemove({
            _id: req.params.id
        });
        if (deleted) {
            await ActionLog.create({
                performer: user._id,
                target: deleted._id,
                type: ActionLogType.DELETE
            });
        }
        res.status(httpStatus.CREATED).send({sremp: deleted ? deleted._id : null});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
