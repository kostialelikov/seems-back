import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {ActionLog, SREMP} from "../../../db";
import httpStatus from "http-status";
import {ActionLogType, IUser} from "../../../db/interfaces";

export const validationSchema = {
    body: {
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        middleName: Joi.string().required(),
        encumbranceType: Joi.string().required(),
        encumbranceModel: Joi.string().required(),
        encumbranceReason: Joi.string().required(),
        encumbranceObject: Joi.string().required(),
        registrationType: Joi.string().required(),
        registrationDate: Joi.date().required(),
        registrationAgency: Joi.string().required(),
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const user = req.user as IUser;
        const body = req.body;
        const created = await SREMP.create({
            ...body,
            registerer: user._id
        });
        await ActionLog.create({
            performer: user._id,
            target: created._id,
            type: ActionLogType.CREATE
        });
        res.status(httpStatus.CREATED).send({sremp: created._id});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
