import {Router} from 'express';
import authRoutes from './auth';
import userRoutes from './user';
import srempRouter from './sremp';
import readRequestRouter from './read_request';
import {badRequest, clientErrorHandler, healthCheck, logErrors, snsSesReports} from './common';
import bodyParser from 'body-parser'

const router = Router();

router.use("/auth", authRoutes);
router.use("/user", userRoutes);
router.use("/sremp", srempRouter);
router.use("/read_request", readRequestRouter);

/* common */
router.get("/health-check", healthCheck);
router.post("/ses-reports", bodyParser.text(), snsSesReports);
router.use(logErrors);
router.use(clientErrorHandler);
router.use(badRequest);

export default router;