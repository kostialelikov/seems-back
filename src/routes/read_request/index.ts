import {Router} from "express";
import passport from "passport";
import validateRole from "../../middlewares/validateRole";
import {UserRole} from "../../db/interfaces";
import {validate} from "express-validation";
import * as create from './handlers/create'
import * as approve from './handlers/approve';
import * as reject from './handlers/reject';
import * as get from './handlers/get';
import * as getAll from './handlers/get_all';

const router = Router();

router.post('/',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.USER]),
    // validate(create.validationSchema),
    create.handler
);

router.put('/approve/:id',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.REGISTERER, UserRole.ADMIN]),
    // validate(approve.validationSchema),
    approve.handler
);

router.put('/reject/:id',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.REGISTERER, UserRole.ADMIN]),
    // validate(reject.validationSchema),
    reject.handler
);

router.get('/:id',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.REGISTERER, UserRole.ADMIN, UserRole.USER]),
    // validate(get.validationSchema),
    get.handler
);

router.get('/',
    passport.authenticate('jwt', {session: false}),
    validateRole([UserRole.REGISTERER, UserRole.ADMIN, UserRole.USER]),
    // validate(getAll.validationSchema),
    getAll.handler
);


export default router;

