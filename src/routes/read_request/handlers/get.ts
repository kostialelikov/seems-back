import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {ReadRequest} from "../../../db";
import httpStatus from "http-status";
import {IUser, UserRole} from "../../../db/interfaces";

export const validationSchema = {
    params: {
        id: Joi.string().required()
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const user = req.user as IUser;
        let readRequest;
        if (user.role === UserRole.USER) {
            readRequest = await ReadRequest.findOne({
                _id: req.params.id,
                reporter: user._id
            }).populate(['reporter', 'target']);
        } else {
            readRequest = await ReadRequest.findOne({
                _id: req.params.id
            }).populate(['reporter', 'target']);
        }
        res.status(httpStatus.CREATED).send({read_request: readRequest ? readRequest.toJSON() : null});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
