import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {ReadRequest} from "../../../db";
import httpStatus from "http-status";
import {IUser} from "../../../db/interfaces";

export const validationSchema = {
    body: {
        filters: {
            firstName: Joi.string().optional().allow(''),
            lastName: Joi.string().optional().allow(''),
            middleName: Joi.string().optional().allow(''),
            id: Joi.string().optional().allow('')
        }
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const user = req.user as IUser;
        const body = req.body;
        console.log(body);
        const created = await ReadRequest.create({
            filters: {
                ...body
            },
            reporter: user._id
        });
        res.status(httpStatus.CREATED).send({read_request: created._id});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
