import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {ReadRequest, SREMP} from "../../../db";
import httpStatus from "http-status";
import {IUser, UserRole} from "../../../db/interfaces";
import {Types} from "mongoose";

export const validationSchema = {
    query: {
        perPage: Joi.number().min(1).default(7),
        page: Joi.number().min(1).default(1),
        search: Joi.string().optional().allow('').default('')
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const user = req.user as IUser;
        const procedures = [];
        if (req.query.search) {
            procedures.push({
                $match: {
                    $or: [
                        {'filters.firstName': {$substr: req.query.search}},
                        {'filters.lastName': {$substr: req.query.search}},
                        {'filters.middleName': {$substr: req.query.search}},
                        {'filters.id': {$substr: req.query.search}}
                    ]
                }
            })
        }
        if (user.role === UserRole.USER) {
            procedures.push({
                $match: {
                    reporter: Types.ObjectId(user._id)
                }
            })
        }
        procedures.push({
            $lookup: {
                from: 'users',
                as: 'reporter',
                foreignField: '_id',
                localField: 'reporter'
            }
        })
        procedures.push({
            $lookup: {
                from: 'sremps',
                as: 'target',
                foreignField: '_id',
                localField: 'target'
            }
        })
        const read = await ReadRequest.aggregate([
            ...procedures,
            {$skip: (Number(req.query.page) - 1) * Number(req.query.perPage)},
            {$limit: Number(req.query.perPage)}
        ]);
        const total = await ReadRequest.aggregate([
            ...procedures,
            {
                $count: 'count'
            }
        ]);

        res.status(httpStatus.CREATED).send({readRequests: read, ...req.query, total: (total && total[0]) ? total[0].count : 0});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
