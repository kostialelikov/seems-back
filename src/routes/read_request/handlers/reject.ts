import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {ReadRequest} from "../../../db";
import httpStatus from "http-status";
import {ReadRequestStatus} from "../../../db/interfaces";

export const validationSchema = {
    params: {
        id: Joi.string().required()
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const updated = await ReadRequest.findOneAndUpdate({
            _id: req.params.id
        }, {
            status: ReadRequestStatus.REJECTED
        });
        res.status(httpStatus.CREATED).send({read_request: updated ? updated._id : null});
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
