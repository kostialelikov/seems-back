import {RequestHandler} from "express";
import Joi from '@hapi/joi';
import {ReadRequest, SREMP} from "../../../db";
import httpStatus from "http-status";
import {ReadRequestStatus} from "../../../db/interfaces";
import {Types} from "mongoose";

export const validationSchema = {
    params: {
        id: Joi.string().required()
    },
    body: {
        target: Joi.string().required()
    }
};

export const handler: RequestHandler = async (req, res) => {
    try {
        const read = await ReadRequest.findOne({
            _id: req.params.id
        });
        if (!read) {
            return res.status(httpStatus.BAD_REQUEST).send('Read Request not found');
        }
        const sremp = await SREMP.findOne({
            $or: [
                {firstName: read.filters.firstName},
                {lastName: read.filters.lastName},
                {middleName: read.filters.middleName},
            ]
        });
        if (!sremp) {
            return res.status(httpStatus.BAD_REQUEST).send('SREMP not found');
        }
        const updated = await ReadRequest.findOneAndUpdate({
            _id: req.params.id
        }, {
            target: sremp._id,
            status: ReadRequestStatus.APPROVED
        });
        res.status(httpStatus.CREATED).send({read_request: updated ? updated._id : null});
    } catch (err) {
        console.log(err);
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
}
