import {connectMongoDB, User} from "./db";
import {encryptPassword} from "./utils/encrypt";

connectMongoDB().then(() => {
    return encryptPassword('admin');
}).then(password => {
    return User.create({
        firstName: 'Admin',
        lastName: 'Admin',
        email: 'admin@sremp.com',
        password
    });
})