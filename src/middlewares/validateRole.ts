import {RequestHandler} from "express";
import {IUser, UserRole} from "../db/interfaces";
import {User} from "../db";

const validateRole = (roles: UserRole[]): RequestHandler => {
    return async (req, res, next) => {
        const user = req.user as IUser;
        const u = await User.findOne({
            _id: user._id
        });
        if (!u || !roles.includes(u.role)) {
            return next(new Error('User not permitted'));
        }
        next();
    };
}

export default validateRole;