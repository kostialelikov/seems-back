import {Document} from "mongoose";
import {IUser} from "./user";
import {ISREMP} from "./sremp";

export enum ReadRequestStatus {
    APPROVED = 'approved',
    REJECTED = 'rejected',
    PENDING = 'pending'
}

export interface IReadRequest extends Document {
    reporter: IUser | string;
    target?: ISREMP | string;
    status: ReadRequestStatus;
    filters: {
        firstName?: string;
        lastName?: string;
        middleName?: string;
        id?: string;
    },
    createdAt: Date;
}