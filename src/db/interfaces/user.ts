import {Document} from "mongoose";

export enum UserRole {
    ADMIN = 'admin',
    REGISTERER = 'registerer',
    USER = 'user'
}

export interface IUser extends Document {
    email: string;
    password: string;
    role: UserRole;
    createdAt: Date;
    updatedAt: Date;
}