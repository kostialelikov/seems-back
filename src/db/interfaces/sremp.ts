import {Document} from "mongoose";
import {IUser} from "./user";

export interface ISREMP extends Document{
    firstName: string;
    lastName: string;
    middleName: string;
    encumbranceType: string;
    encumbranceModel: string;
    encumbranceReason: string;
    encumbranceObject: string;
    registrationType: string;
    registrationDate: Date;
    registrationAgency: string;
    createdAt: Date;
    updatedAt: Date;
    registerer: IUser | string;
}