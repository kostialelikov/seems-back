import {Document} from "mongoose";
import {IUser} from "./user";
import {ISREMP} from "./sremp";

export enum ActionLogType {
    CREATE = 'create',
    DELETE = 'delete',
    UPDATE = 'update'
}

export interface IActionLog extends Document {
    performer: IUser | string;
    target: ISREMP | string;
    key?: string;
    oldValue?: string;
    newValue?: string;
    type: ActionLogType;
    createdAt: Date;
}