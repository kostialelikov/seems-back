import mongoose, {Mongoose} from "mongoose";
import {UserSchema, SREMPSchema, ActionLogSchema, ReadRequestSchema} from "./schemas";
import {IUser, ISREMP, IActionLog, IReadRequest} from "./interfaces";

let mongo: Mongoose;

export const connectMongoDB = async (uri?: string): Promise<Mongoose> => {
    mongo = await mongoose.connect(uri || process.env.MOGNO_URI || '', {useNewUrlParser: true});
    return mongo;
}

export const getMongo = () => {
    return mongo;
}

export const User = mongoose.model<IUser>('user', UserSchema);
export const SREMP = mongoose.model<ISREMP>('sremp', SREMPSchema);
export const ActionLog = mongoose.model<IActionLog>('action_log', ActionLogSchema);
export const ReadRequest = mongoose.model<IReadRequest>('read_request', ReadRequestSchema);
