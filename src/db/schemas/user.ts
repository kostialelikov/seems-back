import {Schema, SchemaTypes} from "mongoose";
import {IUser} from "../interfaces";

export const UserSchema = new Schema({
    email: {
        type: SchemaTypes.String,
        required: true
    },
    firstName: {
        type: SchemaTypes.String,
        required: true
    },
    lastName: {
        type: SchemaTypes.String,
        required: true
    },
    password: {
        type: SchemaTypes.String,
        required: true
    },
    role: {
        type: SchemaTypes.String,
        default: 'user',
        enum: ['user', 'registerer', 'admin']
    },
    createdAt: {
        type: SchemaTypes.Date,
        default: Date.now
    },
    updatedAt: {
        type: SchemaTypes.Date,
        default: Date.now
    }
});

UserSchema.pre<IUser>('save', function (next) {
    this.updatedAt = new Date();
    next();
});