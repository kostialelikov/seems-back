import {Schema, SchemaTypes} from "mongoose";
import {ISREMP} from "../interfaces";

export const SREMPSchema = new Schema({
    firstName: {
        type: SchemaTypes.String,
        required: true
    },
    lastName: {
        type: SchemaTypes.String,
        required: true
    },
    middleName: {
        type: SchemaTypes.String,
        required: true
    },
    encumbranceType: {
        type: SchemaTypes.String,
        required: true
    },
    encumbranceModel: {
        type: SchemaTypes.String,
        required: true
    },
    encumbranceReason: {
        type: SchemaTypes.String,
        required: true
    },
    encumbranceObject: {
        type: SchemaTypes.String,
        required: true
    },
    registrationType: {
        type: SchemaTypes.String,
        required: true
    },
    registrationDate: {
        type: SchemaTypes.String,
        required: true
    },
    registrationAgency: {
        type: SchemaTypes.Date,
        required: true,
    },
    createdAt: {
        type: SchemaTypes.Date,
        default: Date.now
    },
    updatedAt: {
        type: SchemaTypes.Date,
        default: Date.now
    },
    registerer: {
        type: SchemaTypes.ObjectId,
        ref: 'users',
        required: true
    }
});

SREMPSchema.pre<ISREMP>('save', function (next) {
    this.updatedAt = new Date();
    next();
});