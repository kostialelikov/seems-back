import {Schema, SchemaTypes} from "mongoose";

export const ActionLogSchema = new Schema({
    performer: {
        type: SchemaTypes.ObjectId,
        ref: 'users',
        required: true
    },
    target: {
        type: SchemaTypes.ObjectId,
        ref: 'sremps',
        required: true
    },
    key: {
        type: SchemaTypes.String
    },
    oldValue: {
        type: SchemaTypes.String
    },
    newValue: {
        type: SchemaTypes.String
    },
    type: {
        type: SchemaTypes.String,
        required: true,
        enum: ['create', 'delete', 'update']
    },
    createdAt: {
        type: SchemaTypes.Date,
        default: Date.now
    }
});