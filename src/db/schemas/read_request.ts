import {Schema, SchemaTypes} from "mongoose";

export const ReadRequestSchema = new Schema({
    reporter: {
        type: SchemaTypes.ObjectId,
        ref: 'users',
        required: true
    },
    target: {
        type: SchemaTypes.ObjectId,
        ref: 'sremps'
    },
    status: {
        type: SchemaTypes.String,
        required: true,
        default: 'pending',
        enum: ['approved', 'rejected', 'pending']
    },
    filters: {
        firstName: {
          type: SchemaTypes.String
        },
        lastName: {
            type: SchemaTypes.String
        },
        middleName: {
            type: SchemaTypes.String
        },
        id: {
            type: SchemaTypes.String
        },
    },
    createdAt: {
        type: SchemaTypes.Date,
        default: Date.now
    }
});